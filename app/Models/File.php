<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'mime_type',
        'type',
        'path',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'path'
    ];


    protected $appends = [
        'url',
    ];

    public function getUrlAttribute()
    {
        $url = env('APP_URL');
        $url = ['/api', 'files', 'download', $this->name];
        $url = implode('/', $url);
        return $url;
    }
}
