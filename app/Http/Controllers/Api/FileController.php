<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\FileService;
use Illuminate\Http\Request;

class FileController extends Controller
{
    private $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    public function upload(Request $request)
    {
        $file = $request->all();
        $file = $file['file'];
        return $this->ok($this->service->upload($file));
    }

    public function download(string $name)
    {
        return $this->service->download($name);
    }
}
