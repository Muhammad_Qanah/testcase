<?php

namespace App\Http\Controllers\Api;

use App\Dtos\SearchQuery;
use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Services\BookService;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    private $service;

    public function __construct(BookService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\SearchRequest
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        return $this->ok($this->service->search(SearchQuery::fromJson($request->all())));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return $this->ok($this->service->get($id));
    }

    public function store(StoreBookRequest $request)
    {
        return $this->ok($this->service->create($request->all()));
    }

    public function update(UpdateBookRequest $request, int $id)
    {
        return $this->ok($this->service->save($id, $request->all()));
    }

    public function update_publish(Request $request, int $id)
    {
        $validated = $request->validate([
             "comment_approved" => "required|string|boolean",
        ]);
        return $this->ok($this->service->update_publish($id, $request->all()));
    }


    public function destroy(int $id)
    {
        return $this->ok($this->service->delete($id));
    }

}

