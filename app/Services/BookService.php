<?php

namespace App\Services;

use App\Models\Book;

class BookService extends ModelService
{
    /**
     * storable field is a field which can be filled during creating the record
     */
    protected array $storables = [
        'user_id',
        'title',
        'author',
        'photo_id',
        'publication',
        'description'
    ];

    /**
     * updatable field is a field which can be filled during updating the record
     */
    protected array $updatables = [
        'user_id',
        'title',
        'author',
        'photo_id',
        'publication',
        'description'];

    /**
     * searchable field is a field which can be search for from keyword parameter in search method
     */
    protected array $searchables = ['user_id',
        'title',
        'author',
        'photo_id',
        'publication',
        'description'];
    /**
     *
     */
    protected array $with = ['photo'];

    public function builder(): \Illuminate\Database\Eloquent\Builder
    {
        return Book::query();
    }

    /**
     * prepare
     */
    protected function prepare(string $operation, array $attributes): array
    {
        return parent::prepare($operation, $attributes);
    }


    /**
     * create a new book
     */
    public function create(array $attributes)
    {
        $attributes["user_id"]=auth()->user()?auth()->user()->id:0;
        return parent::create($attributes);
    }

    public function save($id, array $attributes)
    {
        return parent::save($id, $attributes);
    }

    public function update_publish($id, array $attributes)
    {

        return parent::save($id, $attributes);
    }
}
