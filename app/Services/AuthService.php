<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthService extends Service
{
    /**
     * login
     */


    public function login(array $credentials)
    {
        $user = User::query()->where('email', '=', $credentials['email'])->get();
        if (isset($user[0])) {
            $user = $user[0];
            if ($user instanceof User) {

                if (Auth::attempt($credentials))

                    $token = $user->createToken('*');
                $data = [
                    'user' => $user->toLightWeightArray(),
                    'token' => $token->plainTextToken,
                ];
                return $this->ok($data, 'auth:login:succeed');
            }
        }
        throw new \Exception('auth:login:errors:credentials');
    }

    public function me()
    {
        $user = auth()->user();

        if ($user instanceof User) {
            return $this->ok($user->toLightWeightArray(), 'auth:me:done');
        }
        throw new \Exception('auth:me:errors:unauthenticated');

    }

    public function logout()
    {
        $user = auth()->user();
        if ($user instanceof User) {
            $user->tokens()->delete();
            return $this->ok(true, 'auth:logout:done');
        }

        throw new \Exception('auth:logout:errors:unauthenticated');
    }
}
