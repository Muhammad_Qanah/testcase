<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserMetaRole;
use App\Models\Vendor;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class UserService extends ModelService
{
    /**
     * storable field is a field which can be filled during creating the record
     */
    protected array $storables =['name', 'email', 'password', 'avatar_id'];
    /**
     * updatable field is a field which can be filled during updating the record
     */
    protected array $updatables = ['name', 'email', 'password', 'avatar_id'];
    /**
     * searchable field is a field which can be search for from keyword parameter in search method
     */
    protected array $searchables = ['name', 'email', 'password', 'avatar_id'];
    /**
     *
     */

    public function __construct( )
    {
    }

    protected array $with = [];

    public function builder(): \Illuminate\Database\Eloquent\Builder
    {
        return User::query();
    }
    /**
     * prepare
     */
    protected function prepare(string $operation, array $attributes): array
    {
        if (isset($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
        }
        return parent::prepare($operation, $attributes);
    }
    public function register($attributes)
    {

        $user = $this->store($attributes);
        if ($user instanceof User) {
            $user = $this->ignoredFind($user->id);
            $token = $user->createToken('*');
            $data = [
                'user' => $user->toLightWeightArray(),
                'token' => $token->plainTextToken,
            ];
            if ($user instanceof User) {
                return $this->ok($data, 'clients:register:step1:done');
            }
        }
        throw new \Exception('clients:register:step1:errors:failed');


    }


    /**
     * create a new user
     */
    public function store(array $attributes): User
    {
        $record = parent::store($attributes);
        // TODO: sites attribute value
        if ($record instanceof User) {
        }
        return $record;
    }

    /**
     *update user
     *
     */
    public function update2($id, array $attributes)
    {
        $fields = $this->prepare('update', $attributes);
        $record = $this->find($id);

        if (isset($attributes["user_phone"])) {
            if ($record instanceof User) {
                if ($record->hasPhone()){

                $record->phone()->update(['meta_value' => $attributes["user_phone"]]);
                }
                else{
                    $record->meta()->create(["user_id" => $record->ID, "meta_key" => "user_phone", "meta_value" => $attributes["user_phone"]]);

                }

            }
            $record = parent::update($id, $attributes);
        }
            $data = $record->toLightWeightArray();


        return $this->ok($data, "users:update:done");
    }

    /**
     * delete inner
     */

    public function destroy($id)
    {
        $record = $this->find($id);
        if ($record instanceof Vendor) {
            $addresses = $record->addresses()->get()->all();
            foreach ($addresses as $address) {
                if ($address instanceof \App\Models\Address) {
                    $address->delete();
                }
            }
        }
        return parent::destroy($id);
    }

    /**
     * activate user
     */
    public function activate(int $id)
    {
        $user = $this->find($id);
        if (!in_array($user->status, [User::status_unverified, User::status_suspended])) {
            throw new \Exception('users:activate:errors:bad');
        }
        $user->status = User::status_active;
        $user->saveOrFail();
        return $this->ok($id, 'users:activate:done');
    }

    /**
     * suspend user
     */
    public function suspend(int $id)
    {
        $user = $this->find($id);
        if (!in_array($user->status, [User::status_active])) {
            throw new \Exception('users:suspend:errors:base');
        }
        $user->status = User::status_suspended;
        $user->saveOrFail();
        return $this->ok($id, 'users:suspend:done');
    }

    /**
     *
     */
    public function ignoredFind($id): User
    {
        $qb = $this->builder()->withoutGlobalScope('accessDB');
        if ($id == null) {
            throw new \Exception('records:find:errors:not_found');
        } else if (is_array($id)) {
            if (!count($id)) {
                throw new \Exception('records:find:errors:not_found');
            }
            foreach ($id as $k => $value) {
                $qb = $qb->where($k, '=', $value);
            }
        } else {
            $qb = $qb->where('id', '=', $id);
        }
        $qb = $qb->first();
        if (!$qb instanceof User) {
            throw new \Exception('records:find:errors:not_found');
        }
        return $qb;
    }
}
