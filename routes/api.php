<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('/auth')->group(function () {
    //
    Route::post('/login', 'AuthController@login');
    Route::post('/register', 'UserController@register');
    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/me', 'AuthController@me');
        Route::post('/logout', 'AuthController@logout');
    });
});
Route::prefix('/books')->group(function () {
    //
    Route::get('/', 'BooksController@index');

    Route::get('/{id}', 'BooksController@show');
    Route::middleware('auth:sanctum')->group(function () {
        Route::post('/update_publish/{id}', 'BooksController@update_publish');
        Route::post('/', 'BooksController@store');
        Route::put('/{id}', 'BooksController@update');
        Route::delete('/{id}', 'BooksController@destroy');
    });
});


Route::prefix('/files')
    ->group(function () {
        Route::middleware('auth:sanctum')->group(function () {
            Route::post('/upload', 'FileController@upload');
        });
        Route::get('/download/{name}', 'FileController@download');
    });
