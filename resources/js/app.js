/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from "vue";
import 'bootstrap/dist/css/bootstrap.min.css'
import router from "./router";


require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/HomeComponent.vue -> <example-component></example-component>
 */

Vue.component('home-component', require('./components/Widgets/HomeComponent.vue').default);
Vue.component('book-view', require('./components/Widgets/BookView.vue').default);
Vue.component('sign-in', require('./components/Widgets/SignIn.vue').default);
import Default_Layout from "./layouts/Default.vue";
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";

Vue.use(Antd);
Vue.component("layout-default", Default_Layout);
Vue.config.productionTip = true
import VueRouter from "vue-router";

Vue.use(VueRouter);
import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
import authHeader from "./services/auth-header";
import helper from "./helper";
import VueAxios from "vue-axios";
import store from "./store";

const axios = require("axios");
Vue.use(VueAxios, axios);
axios.interceptors.request.use(function (config) {
    // console.log(authHeader())
    config.headers["Accept"] = "application/json";
    config.headers["Authorization"] = store.getters.StateAuthorization;
    return config;
});
axios.defaults.withCredentials = true
Vue.use(VueSweetalert2);
const app = new Vue({
    el: '#app',
    router,
    Antd,
    helper,
    store
});
