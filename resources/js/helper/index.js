import Vue from 'vue'
import VueCookies from 'vue-cookies'

Vue.use(VueCookies);
const axios = require('axios')

class Helper {
    url;

    get(params) {
        this.url = "sdddsd";
    }

    post() {

    }

    put() {

    }

    delete() {

    }

    login(user) {
        return axios
            .post('auth/login', {
                user_email: user.user_email,
                user_pass: user.user_pass
            })
            .then(response => {
                // console.log(response.data)
                if (response.data.data.token) {
                    // localStorage.setItem('user', JSON.stringify(response.data.data.user));
                    // localStorage.setItem('role', JSON.stringify(response.data.data.user.role.name));
                    localStorage.setItem('accessToken', JSON.stringify(response.data.data.token));
                    localStorage.setItem('user', JSON.stringify(response.data.data.user));

                    if (JSON.stringify(response.data.data.user.role)) {
                        localStorage.setItem('role', JSON.stringify(response.data.data.user.role.id));
                    }
                    document.cookie = "accessToken=" + JSON.stringify(response.data.data.token);
                    document.cookie = "user=" + JSON.stringify(response.data.data);
                }
                return response;
            });
    }

    logout() {
        return axios.post('auth/logout')
            .then(resp => {
                localStorage.removeItem('accessToken')
                localStorage.removeItem('user')


                return resp
            })
            .catch(err => {
                localStorage.removeItem('accessToken')
                localStorage.removeItem('user')
                return err
            })
    }

    register(user) {
        return axios.post('auth/register', {
            name: user.name,
            email: user.email,
            password: user.password,
            password_confirmation: user.password_confirmation
        });
    }
}

export default new Helper();
