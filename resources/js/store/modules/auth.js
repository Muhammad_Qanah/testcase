//store/modules/auth.js

import axios from 'axios';
import {commit} from "lodash";

const state = {
    user: null,
    Authorization: null,
};
const getters = {
    isAuthenticated: state => !!state.user,
    StateAuthorization: state => state.Authorization,
    StateUser: state => state.user,

};
const actions = {
    async Register({commit}, form) {
        let response = await axios.post('/api/auth/register', form)
        await commit('setUser', response.data.data.user)
        await commit('setAuthorization', 'Bearer ' + response.data.data.token)
    },
    async LogIn({commit}, data) {
        let response = await axios.post('/api/auth/login', data)
        await commit('setUser', response.data.data.user)
        await commit('setAuthorization', 'Bearer ' + response.data.data.token)
    },
    async LogOut({commit}) {
        let user = null
        commit('logout', user)
    }
};
const mutations = {
    setUser(state, email) {
        state.user = email
    },
    setAuthorization(state, authorization) {
        state.Authorization = authorization
    },
    LogOut(state) {
        state.user = null
        state.authorization = null
    },
};
export default {
    state,
    getters,
    actions,
    mutations
};
