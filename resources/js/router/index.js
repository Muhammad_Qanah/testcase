import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "../store";
Vue.use(VueRouter)

let routes = [
    {
        // will match everything
        path: '*',
        component: () => import('../views/HomePage.vue'),
    },
    {
        path: '/',
        component: () => import('../views/HomePage.vue'),

    },
    {
        path: '/sign-in',
        component: () => import('../views/SignIn.vue'),
    },   {
        path: '/sign-up',
        component: () => import('../views/SignUp.vue'),
    },
    {
        path: '/book',
        component: () => import('../views/book/book.vue'),
        meta: { requiresAuth: true },
    }
    ,{
        path: '/book/new',
        component: () => import('../views/book/new.vue'),
        meta: { requiresAuth: true },
    }
    ,{
        path: '/books/:id',
        component: () => import('../views/book/edit.vue'),
        meta: { requiresAuth: true },
    }  ,{
        path: '/books/view/:id',
        component: () => import('../views/book/view.vue'),
        meta: { requiresAuth: true },
    }

]

function addLayoutToRoute(route, parentLayout = "default") {
    route.meta = route.meta || {};
    route.meta.layout = route.layout || parentLayout;

    if (route.children) {
        route.children = route.children.map((childRoute) => addLayoutToRoute(childRoute, route.meta.layout));
    }
    return route;
}

routes = routes.map((route) => addLayoutToRoute(route));

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
    scrollBehavior(to) {
        if (to.hash) {
            return {
                selector: to.hash,
                behavior: 'smooth',
            }
        }
        return {
            x: 0,
            y: 0,
            behavior: 'smooth',
        }
    }

});
router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (store.getters.isAuthenticated) {
            next();
            return;
        }
        next("/sign-in");
    } else {
        next();
    }
});

export default router
