export default function authHeader() {
    let accessToken = JSON.parse(localStorage.getItem('accessToken'));
    let user = JSON.parse(localStorage.getItem('user'));
    //console.log(accessToken)
    if (accessToken) {
        return {Authorization: 'Bearer ' + accessToken, User: user};
    } else {
        return {};
    }
}