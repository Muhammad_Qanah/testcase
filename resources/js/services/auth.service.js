import Vue from 'vue'
import VueCookies from 'vue-cookies'
Vue.use(VueCookies);
const axios = require('axios')

class AuthService {
    login(user) {
        return axios
            .post('/api/auth/login', {
                email: user.email,
                password: user.password
            })
            .then(response => {
                if (response.data.data.token) {
                    localStorage.setItem('accessToken', JSON.stringify(response.data.data.token));
                    localStorage.setItem('user', JSON.stringify(response.data.data.user));
                    if (JSON.stringify(response.data.data.user.role)) {
                    }
                    document.cookie = "accessToken=" + JSON.stringify(response.data.data.token);
                    document.cookie = "user=" + JSON.stringify(response.data.data);
                }
                return response;
            });
    }
    logout() {
        return axios.post('api/auth/logout')
            .then(resp => {
                localStorage.removeItem('accessToken')
                localStorage.removeItem('user')


                return resp
            })
            .catch(err => {
                localStorage.removeItem('accessToken')
                localStorage.removeItem('user')
                return err
            })
    }

    register(user) {
        return axios.post('api/auth/register', {
            name: user.name,
            email: user.email,
            password: user.password,
            password_confirmation: user.password_confirmation
        });
    }
}

export default new AuthService();
